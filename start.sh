#!/bin/bash

cd /src

if [ ! -f "/src/node_modules" ]; then
  echo "npm packages installing..."
  npm install --silent
  echo "npm packages installation is done"
fi

npm start
